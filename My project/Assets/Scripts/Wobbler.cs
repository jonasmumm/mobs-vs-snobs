using UnityEngine;

namespace AGBIC2021.Scripts.Misc
{
    public class Wobbler
    {
        public float target;
        public float current;
        public float dampingLambda;
        public float acceleration;
        public float speed;
        bool idleWobbleProtection;

        public Wobbler(float current, float target, float acceleration, float dampingLambda, float speed, bool idleWobbleProtection = true)
        {
            this.target = target;
            this.current = current;
            this.dampingLambda = dampingLambda;
            this.acceleration = acceleration;
            this.speed = speed;
            this.idleWobbleProtection = idleWobbleProtection;
        }

        public float Update(float deltaTime)
        {
            var delta = target - current;

            if (delta == 0f && Mathf.Abs(speed) < deltaTime * acceleration * 0.25f) return current;

            var maxAccel = idleWobbleProtection
                ? Mathf.Min(Mathf.Abs(delta), deltaTime * acceleration)
                : deltaTime * acceleration;

            speed += Mathf.Sign(delta) * maxAccel;

            speed = Damp(speed, 0f, dampingLambda, deltaTime);
            current += speed * deltaTime;

            return current;
        }

        public static float Damp(float a, float b, float lambda, float dt)
        {
            return Mathf.Lerp(a, b, 1f - Mathf.Exp(-lambda * dt));
        }
    }
}
