using UnityEngine;

public class Pulsate : MonoBehaviour
{
    [SerializeField] Vector3 max;
    [SerializeField] Vector3 min;
    [SerializeField, Min(0.001f)] float frequency;
    [SerializeField] bool randomizeOffset;

    float _offset = 0;

    private void OnEnable()
    {
        if (randomizeOffset)
        {
            _offset = UnityEngine.Random.Range(0f, 1f / frequency);
        }
    }

    private void Update()
    {
        var lerp = 0.5f + 0.5f * Mathf.Sin((Time.time + _offset) * Mathf.PI * frequency);
        transform.localScale = Vector3.Lerp(min, max, lerp);
    }
}
