using UnityEngine;

public class FaceCamera : MonoBehaviour
{
    [SerializeField] Quaternion localOffset;
    private Camera _cam;

    private void Awake()
    {
    }

    private void Update()
    {
        if(!_cam) _cam = Camera.main;
        transform.rotation = _cam.transform.rotation;
        transform.localRotation *= localOffset;
    }
}
