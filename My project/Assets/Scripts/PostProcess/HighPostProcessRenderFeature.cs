using UnityEngine;
using UnityEngine.Rendering.Universal;

[System.Serializable]
public class HighPostProcessRenderFeature : ScriptableRendererFeature
{
    static readonly int IntensityShaderId = Shader.PropertyToID("_Intensity");

    [SerializeField] Material material;

    public static HighPostProcessRenderFeature Instance { get; private set; }

    Material _material;
    HighPostProcessPass _pass;

    public override void Create()
    {
        Instance = this;
        _material = new Material(material);
        _pass = new HighPostProcessPass(_material);
    }

    public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
    {
        renderer.EnqueuePass(_pass);
    }

    public void SetIntensity(float intensity)
    {
        _material.SetFloat(IntensityShaderId, intensity);
    }
}
