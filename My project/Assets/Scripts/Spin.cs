using UnityEngine;

public class Spin : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] Vector3 perAxisMultiplier = new Vector3(0f, 1f, 0f);

    Quaternion _baseLocalRotation;

    private void Awake()
    {
        _baseLocalRotation = transform.localRotation;
    }

    private void Update()
    {
        var rot = speed * Time.time;
        transform.localRotation = _baseLocalRotation * Quaternion.Euler(rot * perAxisMultiplier.y, rot * perAxisMultiplier.y, rot * perAxisMultiplier.y);
    }
}
