#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;

public static class EditorSceneUtils
{
    [MenuItem("Mobs vs Snobs/Start from Beginning")]
    public static void StartBootstrap()
    {
        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();

        EditorSceneManager.OpenScene("Assets/Scenes/Bootstrap/Bootstrap.unity", OpenSceneMode.Single);
        EditorApplication.EnterPlaymode();
    }
}
#endif
