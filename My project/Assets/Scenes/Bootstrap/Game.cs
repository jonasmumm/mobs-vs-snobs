using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{
    [SerializeField] HighSwitcher highSwitcher;

    public static HighSwitcher HighSwitcher => _instance.highSwitcher;

    static Game _instance;

    static readonly Dictionary<string, Scene> _openSceneName2Scene = new Dictionary<string, Scene>();

    private void Awake()
    {
        Init();
    }

    private void Update()
    {
        if (Input.GetButton("Pause"))
        {
            Application.Quit();
        }
    }

    public void Init()
    {
        _instance = this;
    }

    public static void OpenScene<TScene>(bool setActive, Action<TScene> beforeOpenCallback = null, string nameOverride = null) where TScene : MonoBehaviour
    {
        var sceneName = nameOverride != null
            ? nameOverride
            : typeof(TScene).Name;

        if (!_openSceneName2Scene.TryGetValue(sceneName, out var scene))
        {
            SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive).completed += _ => OnLoaded();
        }
        else
        {
            OnLoaded();
        }

        void OnLoaded()
        {
            var scene = SceneManager.GetSceneByName(sceneName);
            _openSceneName2Scene[sceneName] = scene;

            if (setActive)
            {
                SceneManager.SetActiveScene(scene);
            }

            var roots = scene.GetRootGameObjects();

            if (roots.Length != 1)
            {
                Debug.LogWarning("Scene " + sceneName + " has root count == " + roots.Length);
            }

            var tScene = roots[0].GetComponent<TScene>();

            beforeOpenCallback?.Invoke(tScene);

            foreach (var r in roots)
            {
                r.gameObject.SetActive(true);
            }

            if (!tScene)
            {
                throw new System.Exception("tscene is null " + typeof(TScene).Name);
            }
        }
    }

    public static void CloseScene<TScene>(bool unload, string overrideName = null)
    {
        var sceneName = overrideName != null
            ? overrideName
            : typeof(TScene).Name;

        var scene = _openSceneName2Scene[sceneName];
        var roots = scene.GetRootGameObjects();

        foreach (var r in roots)
        {
            r.gameObject.SetActive(false);
        }

        if (unload)
        {
            SceneManager.UnloadSceneAsync(sceneName);
            _openSceneName2Scene.Remove(sceneName);
        }
    }
}
