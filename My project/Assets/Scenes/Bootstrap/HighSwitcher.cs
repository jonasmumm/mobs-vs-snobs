using System.Collections;
using UnityEngine;

public class HighSwitcher : MonoBehaviour
{
    [SerializeField] AnimationCurve gainCurve;
    [SerializeField] float gainDuration;
    [SerializeField] AnimationCurve removeCurve;
    [SerializeField] float removeDuration;

    float _target;
    Coroutine _coroutine;

    public void SetHigh(float value)
    {
        if (value == _target) return;

        var start = _target;
        var delta = value - _target;
        _target = value;
        var duration = delta > 0
            ? gainDuration
            : removeDuration;
        var curve = delta > 0
            ? gainCurve
            : removeCurve;

        if (_coroutine != null)
        {
            StopCoroutine(_coroutine);
            _coroutine = null;
        }

        _coroutine = StartCoroutine(Transition(start, _target, duration, curve));
    }

    private IEnumerator Transition(float start, float target, float duration, AnimationCurve curve)
    {
        var startTime = Time.time;
        var end = startTime + duration;

        while (Time.time < end)
        {
            var progress = Mathf.InverseLerp(startTime, end, Time.time);
            var v = Mathf.LerpUnclamped(start, target, curve.Evaluate(progress));

            HighPostProcessRenderFeature.Instance.SetIntensity(v);

            yield return null;
        }
    }
}
