using UnityEngine;
using UnityEngine.SceneManagement;

public class Bootstrap : MonoBehaviour
{
    [SerializeField] Game game;

    private void Awake()
    {
        game.Init();
        Game.OpenScene<MainMenu>(false);
    }
}
