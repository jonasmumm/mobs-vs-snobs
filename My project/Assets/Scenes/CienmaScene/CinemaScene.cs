using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class CinemaScene : MonoBehaviour
{
    public enum Type
    {
        Intro,
        GameOver,
        Success
    }

    [Serializable]
    public class Data
    {
        [SerializeField] Type type;
        [SerializeField] Sprite sprite;
        [SerializeField] AudioClip audioClip;
        [SerializeField] float seconds;

        public Sprite Sprite => sprite;
        public Type Type => type;
        public AudioClip AudioClip => audioClip;
        public float Seconds => seconds;
    }

    [SerializeField] Data[] data;
    [SerializeField] Image sprite;
    [SerializeField] AudioSource audioSource;

    Type _type;
    Action _after;

    public void SetData(Type type, Action after)
    {
        _type = type;
        _after = after;
    }

    private void OnEnable()
    {
        var data = this.data.First(v => v.Type == _type);
        sprite.sprite = data.Sprite;
        audioSource.clip = data.AudioClip;
        StartCoroutine(WaitAndThen(data.Seconds, _after));
        audioSource.Play();
    }

    private IEnumerator WaitAndThen(float seconds, Action after)
    {
        yield return new WaitForSeconds(seconds);

        after?.Invoke();
    }
}
