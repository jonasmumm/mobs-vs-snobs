public class DonutCollectable : ACollectable
{
    protected override void OnCollected(Controllable controllable)
    {
        controllable.PlayerController.SetControllable(ControllableType.Dog);
    }
}
