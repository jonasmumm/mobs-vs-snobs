using UnityEngine;

public class WalkController : MonoBehaviour
{
    [SerializeField] float accelerationTorque;
    [SerializeField] Rigidbody rb;
    [SerializeField] Camera cam;

    private void FixedUpdate()
    {
        var axisH = Input.GetAxisRaw("Horizontal");
        var axisV = Input.GetAxisRaw("Vertical");

        //.-


        var input = new Vector3(axisH, 0f, axisV);

        if (input.magnitude == 0f) return;

        input = input.normalized;

        var camForward = cam.transform.forward;
        var camFlatForward = new Vector3(camForward.x, 0f, camForward.z);
        var camRot = Quaternion.LookRotation(camFlatForward);
        var inputRot = Quaternion.LookRotation(input);
        var deltaRot = camRot * inputRot;

        var forceDir = deltaRot * Vector3.forward;
        rb.AddForce(forceDir * accelerationTorque, ForceMode.Acceleration);
        //rb.AddTorque(forceDir * accelerationTorque, ForceMode.Acceleration);
    }
}
