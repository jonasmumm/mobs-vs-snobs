using UnityEngine;

public class SimpleEnemy : MonoBehaviour
{
    [SerializeField] float chargeDuration;
    [SerializeField] float turnLerpAmount;
    [SerializeField] float maxTargetDistance;
    [SerializeField] float pushVelocity;
    [SerializeField] Vector3 maxScale;
    [SerializeField] Vector3 minScale;
    [SerializeField] float scaleExponent;
    [SerializeField] Transform scaleAndShakeContainer;
    [SerializeField] float shakeAmount;
    [SerializeField] float shakeExponent;
    [SerializeField] Rigidbody rb;

    float _currentChargeDuration = 0f;
    private Transform _target;
    Quaternion _lastTarget;

    private void OnCollisionEnter(Collision collision)
    {
        var other = collision.gameObject;

        var controllable = other.GetComponentInChildren<Controllable>();
        if (controllable)
        {
            controllable.PlayerController.GameOver();
        }
    }

    private void Update()
    {
        var dist = transform.position - _target.position;

        if (dist.magnitude > maxTargetDistance)
        {
            Reposition();
        }

        _currentChargeDuration += Time.deltaTime;
        var progress = _currentChargeDuration / chargeDuration;

        scaleAndShakeContainer.localScale = Vector3.LerpUnclamped(minScale, maxScale, Mathf.Pow(progress, scaleExponent));
        var shakeMax = shakeAmount * Mathf.Pow(progress, shakeExponent);
        scaleAndShakeContainer.localPosition = new Vector3(
            UnityEngine.Random.Range(-shakeMax, shakeMax),
            UnityEngine.Random.Range(-shakeMax, shakeMax),
            UnityEngine.Random.Range(-shakeMax, shakeMax));

        if (progress >= 1f)
        {
            _currentChargeDuration = 0f;
            var direction = Quaternion.LookRotation(_target.transform.position - transform.position);
            direction = Quaternion.LerpUnclamped(_lastTarget, direction, turnLerpAmount);
            rb.AddForce(direction * Vector3.forward * pushVelocity, ForceMode.VelocityChange);
            _lastTarget = direction;
        }
    }

    private void Reposition()
    {
        var newDist = maxTargetDistance * 0.85f;
        var randomDir = new Vector3(UnityEngine.Random.Range(-1f, 1f), 0f, Random.Range(-1f, 1f)).normalized;
        transform.position = _target.transform.position + randomDir * newDist + Vector3.up * 3f;
        rb.velocity = Vector3.zero;
    }

    public void SetData(Transform target, PlayerController playerController)
    {
        _target = target;
        Reposition();
        _lastTarget = Quaternion.LookRotation(_target.transform.position - transform.position);
        playerController.ControllableChanged += OnControllableChanged;
    }

    private void OnControllableChanged(Controllable obj)
    {
        gameObject.SetActive(obj.ControllableType == ControllableType.Dog);
    }
}
