using System;
using UnityEngine;

public abstract class ACollectable : MonoBehaviour
{
    [SerializeField] bool spin;
    [SerializeField] float spinSpeed;

    public event Action Collected;

    Quaternion _baseLocalRotation;

    private void Awake()
    {
        _baseLocalRotation = transform.localRotation;
    }

    private void Update()
    {
        if (spin)
        {
            transform.localRotation = _baseLocalRotation * Quaternion.Euler(0f, spinSpeed * Time.time, 0f);
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        var controllable = other.gameObject.GetComponentInParent<Controllable>();

        if (controllable)
        {
            Collect(controllable);
        }
    }

    private void Collect(Controllable controllable)
    {
        OnCollected(controllable);
        Collected?.Invoke();
        Destroy(gameObject);
    }

    protected abstract void OnCollected(Controllable controllable);
}
