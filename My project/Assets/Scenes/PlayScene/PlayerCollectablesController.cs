using UnityEngine;

public class PlayerCollectablesController : MonoBehaviour
{
    [SerializeField] PlayerController playerController;
    [SerializeField] RadiusStuffSpawner radiusSpawner;

    private void Awake()
    {
        playerController.ControllableChanged += OnControllableChanged;
    }

    private void OnControllableChanged(Controllable obj)
    {
        radiusSpawner.SetData(obj.transform, obj.SpawnedCollectableRadius * 0.6f, obj.SpawnedCollectableRadius * 0.9f, obj.SpawnedCollectableRadius, obj.SpawnedCollectableAmount, obj.SpawnedCollectablePrefab.gameObject);
    }
}
