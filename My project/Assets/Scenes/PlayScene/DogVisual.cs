using AGBIC2021.Scripts.Misc;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DogVisual : MonoBehaviour
{
    [SerializeField] Rigidbody rb;
    [SerializeField] Transform[] forwardContainers;
    [SerializeField] Transform[] rollTargets;
    [SerializeField] float rollAmount;

    Wobbler _rollWobbler;

    float _prevRbRot = 0;
    readonly Dictionary<Transform, Quaternion> _defaultLocalRot = new Dictionary<Transform, Quaternion>();

    private void Awake()
    {
        _rollWobbler = new Wobbler(0f, 0f, 80f, 3f, 0f, false);
        _prevRbRot = forwardContainers.First().rotation.eulerAngles.y;

        foreach (var t in rollTargets)
        {
            _defaultLocalRot.Add(t, t.localRotation);
        }
    }

    private void Update()
    {
        UpdateForward();
    }

    private void FixedUpdate()
    {
        UpdateRoll();
    }

    private void UpdateForward()
    {
        var flatVelocity = new Vector3(rb.velocity.x, 0f, rb.velocity.z);
        if (flatVelocity.magnitude < 0.1f) return;

        var rot = Quaternion.LookRotation(flatVelocity);

        foreach (var t in forwardContainers)
        {
            t.rotation = rot;
        }
    }

    void UpdateRoll()
    {
        var rot = forwardContainers.First().rotation.eulerAngles.y;
        var delta = Mathf.DeltaAngle(rot, _prevRbRot);
        _rollWobbler.target = delta;
        _rollWobbler.Update(Time.deltaTime);
        var value = _rollWobbler.current * rollAmount;

        foreach (var t in rollTargets)
        {
            t.localRotation = _defaultLocalRot[t] * Quaternion.Euler(0f, value, 0f);
        }

        _prevRbRot = rot;
    }
}
