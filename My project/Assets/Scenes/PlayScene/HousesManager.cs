using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HousesManager : MonoBehaviour
{
    [SerializeField] House[] houses;
    [SerializeField] Controllable dog;
    [SerializeField] PlayerController playerController;
    [SerializeField] float addEnemyPerSecond;
    [SerializeField] float difficultyExponent;
    [SerializeField] SimpleEnemy enemyPrefab;

    public int Score => _score;
    public float EnemyChargeProgress => _enemyChargeProgress;
    public int EnemiesActive => _spawnedEnemies.Count;

    House _snobbedHouse;
    int _score = 0;
    float _enemyChargeProgress;
    private ControllableType _controllableType;
    readonly List<SimpleEnemy> _spawnedEnemies = new List<SimpleEnemy>();

    private void Awake()
    {
        foreach (var h in houses)
        {
            var house = h;
            h.Pooed += () => OnHousePooedOn(house);
        }

        playerController.ControllableChanged += OnControllableChanged;
    }

    private void OnControllableChanged(Controllable obj)
    {
        _controllableType = obj.ControllableType;
    }

    void OnEnable()
    {
        _score = 0;

        foreach (var h in houses)
        {
            h.SetSnobbed(false);
        }

        AddSnobbedHouse();
    }

    private void Update()
    {
        if (_controllableType != ControllableType.Dog) return;

        var gainPerSecond = addEnemyPerSecond * Mathf.Pow(_score, difficultyExponent);
        _enemyChargeProgress += gainPerSecond * Time.deltaTime;

        if (_enemyChargeProgress >= 1f)
        {
            _enemyChargeProgress -= 1f;
            AddEnemy();
        }
    }

    private void AddEnemy()
    {
        var instance = Instantiate(enemyPrefab, dog.transform.parent);
        instance.SetData(dog.transform, playerController);
        _spawnedEnemies.Add(instance);
    }

    private void AddSnobbedHouse()
    {
        var housesByDistance = houses.OrderBy(v => (v.transform.position - dog.transform.position).magnitude).ToArray();

        var index = UnityEngine.Random.Range(housesByDistance.Length / 2, housesByDistance.Length);
        _snobbedHouse = housesByDistance[index];
        _snobbedHouse.SetSnobbed(true);
    }

    private void OnHousePooedOn(House house)
    {
        if (house != _snobbedHouse) return;

        AddScore();
        AddSnobbedHouse();
    }

    private void AddScore()
    {
        _score++; //increase difficulty
        foreach(var enemy in _spawnedEnemies)
        {
            Destroy(enemy.gameObject);
        }
    }
}
