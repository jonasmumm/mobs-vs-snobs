using System.Collections.Generic;
using UnityEngine;

public class RadiusStuffSpawner : MonoBehaviour
{
    readonly List<GameObject> _spawned = new List<GameObject>();
    private Transform _target;
    private float _minSpawnRadius;
    private float _maxSpawnRadius;
    private float _destroyRadius;
    private int _amount;
    private GameObject[] _prefabs;

    public void SetData(Transform target, float minSpawnRadius, float maxSpawnRadius, float destroyRadius, int amount, params GameObject[] prefabs)
    {
        foreach (var v in _spawned)
        {
            Destroy(v.gameObject);
        }

        _spawned.Clear();

        _target = target;
        _minSpawnRadius = minSpawnRadius;
        _maxSpawnRadius = maxSpawnRadius;
        _destroyRadius = destroyRadius;
        _amount = amount;
        _prefabs = prefabs;
    }

    private void Update()
    {
        if (_prefabs == null || !_target) return;

        if (_spawned.Count < _amount)
        {
            var p = _prefabs[UnityEngine.Random.Range(0, _prefabs.Length)];
            var instance = Instantiate(p, _target.transform.parent);
            var dir = new Vector3(UnityEngine.Random.Range(-999f, 999f), 0f, UnityEngine.Random.Range(-999f, 999f)).normalized;
            var dist = Mathf.Lerp(_minSpawnRadius, _maxSpawnRadius, UnityEngine.Random.Range(0f, 1f));
            instance.transform.position = _target.position + dir * dist;
            var euler = instance.transform.rotation;
            instance.transform.rotation = Quaternion.Euler(euler.x, UnityEngine.Random.Range(0f, 360f), euler.z);
            _spawned.Add(instance);
        }
        else if (_spawned.Count > 0)
        {
            var checkIndex = Time.frameCount % _spawned.Count;

            var collectable = _spawned[checkIndex];

            if (!collectable)
            {
                _spawned.RemoveAt(checkIndex);
                return;
            }

            var dist = (collectable.transform.position - _target.position).magnitude;
            if (dist > _destroyRadius)
            {
                Destroy(collectable.gameObject);
                _spawned.RemoveAt(checkIndex);
            }
        }
    }
}
