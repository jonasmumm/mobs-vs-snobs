using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Controllable : MonoBehaviour
{
    [SerializeField] ControllableType controllableType;
    [SerializeField] MonoBehaviour[] enableComponents;
    [SerializeField] ACollectable spawnedCollectablePrefab;
    [SerializeField] Cinemachine.CinemachineFreeLook freeLook;
    [SerializeField] int spawnedCollectableAmount;
    [SerializeField] float spawnedCollectableRadius;

    public ControllableType ControllableType => controllableType;
    public IReadOnlyList<MonoBehaviour> EnableComponents => enableComponents.Concat(new[] { this }).ToArray();

    public PlayerController PlayerController { get; private set; }

    public ACollectable SpawnedCollectablePrefab => spawnedCollectablePrefab;
    public int SpawnedCollectableAmount => spawnedCollectableAmount;
    public float SpawnedCollectableRadius => spawnedCollectableRadius;
    public Cinemachine.CinemachineFreeLook FreeLook => freeLook;

    internal void SetPlayerController(PlayerController playerController)
    {
        PlayerController = playerController;
    }
}
