using UnityEngine;

public class SnobsLoveThisSpawner : MonoBehaviour
{
    [SerializeField] PlayerController playerController;
    [SerializeField] RadiusStuffSpawner radiusSpawner;
    [SerializeField] GameObject[] prefabs;
    [SerializeField] int amount;
    [SerializeField] float radius;

    private void Awake()
    {
        playerController.ControllableChanged += OnControllableChanged;
    }

    private void OnControllableChanged(Controllable obj)
    {
        radiusSpawner.SetData(obj.transform, radius * 0.4f, radius * 0.7f, radius, amount, prefabs);
    }
}
