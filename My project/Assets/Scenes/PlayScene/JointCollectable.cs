using UnityEngine;

public class JointCollectable : ACollectable
{
    [SerializeField] AudioSource audioSource; 

    protected override void OnCollected(Controllable controllable)
    {
        controllable.PlayerController.SetControllable(ControllableType.Hawk);
    }
}
