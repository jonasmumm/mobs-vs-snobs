using UnityEngine;

public class SnobsLoveThis : MonoBehaviour
{
    [SerializeField] float speedBoostVelocityChange;

    private void OnCollisionEnter(Collision collision)
    {
        var controllable = collision.gameObject.GetComponentInParent<Controllable>();

        if (controllable)
        {
            var rb = controllable.GetComponentInParent<Rigidbody>();
            var forward = rb.velocity;

            rb.AddForce(forward.normalized * speedBoostVelocityChange, ForceMode.VelocityChange);
        }
    }
}
