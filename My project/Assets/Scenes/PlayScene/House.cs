using System;
using UnityEngine;

public class House : MonoBehaviour
{
    [SerializeField] GameObject airZZZSign;
    [SerializeField] Pulsate pulsate;

    public event Action Pooed;

    private bool _isSnobbed;

    void OnEnable()
    {
        if (airZZZSign)
        {
            airZZZSign.gameObject.SetActive(_isSnobbed);
        }
    }

    public void SetSnobbed(bool snobbed)
    {
        _isSnobbed = snobbed;
        if (airZZZSign)
        {
            airZZZSign.gameObject.SetActive(_isSnobbed);
        }

        pulsate.enabled = _isSnobbed;
    }

    internal void HasBeenPooedOn()
    {
        SetSnobbed(false);
        Pooed?.Invoke();
    }
}
