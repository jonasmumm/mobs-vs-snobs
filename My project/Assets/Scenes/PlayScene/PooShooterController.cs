using UnityEngine;

public class PooShooterController : MonoBehaviour
{
    [SerializeField] Poo pooPrefab;
    [SerializeField] Transform pooOrigin;
    [SerializeField] float pooVelocity;
    [SerializeField] Rigidbody rb;

    private void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            var instance = Instantiate(pooPrefab, transform.parent);
            instance.transform.position = pooOrigin.transform.position;
            instance.transform.rotation = pooOrigin.transform.rotation;
            instance.Rb.velocity = rb.velocity;
            instance.Rb.AddForce(pooOrigin.forward * pooVelocity, ForceMode.VelocityChange);
        }
    }
}
