using System;
using System.Linq;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] Controllable[] controllables;
    [SerializeField] ControllableType startType;

    public event Action<Controllable> ControllableChanged
    {
        add
        {
            _controllableChanged += value;
            if (_current) value.Invoke(_current);
        }
        remove => _controllableChanged -= value;
    }
    event Action<Controllable> _controllableChanged;

    Controllable _current;

    public Controllable[] Controllables => controllables;

    private void Awake()
    {
        foreach (var v in controllables)
        {
            foreach (var b in v.EnableComponents)
            {
                b.enabled = false;
            }
        }

        SetControllable(startType);
    }

    internal void GameOver()
    {
        Game.CloseScene<PlayScene>(true);
        Game.OpenScene<MainMenu>(false);
    }

    public void SetControllable(ControllableType controllableType)
    {

        switch (controllableType)
        {
            case ControllableType.Hawk:
                Game.HighSwitcher.SetHigh(1f);
                break;
            case ControllableType.Dog:
                Game.HighSwitcher.SetHigh(0f);
                break;
        }

        var it = controllables.First(v => v.ControllableType == controllableType);

        if (_current)
        {
            foreach (var v in _current.EnableComponents)
            {
                v.enabled = false;
            }

            it.FreeLook.m_XAxis.Value = _current.FreeLook.m_XAxis.Value; // DOES NOT WORK?
            it.FreeLook.m_YAxis.Value = _current.FreeLook.m_YAxis.Value;
        }

        _current = it;
        _current.SetPlayerController(this);

        foreach (var v in _current.EnableComponents)
        {
            v.enabled = true;
        }

        _controllableChanged?.Invoke(_current);
    }
}
