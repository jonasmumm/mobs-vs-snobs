using UnityEngine;

public class Poo : MonoBehaviour
{
    [SerializeField] Rigidbody rb;
    [SerializeField] float maxLifetime;
    private float _start;

    public Rigidbody Rb => rb;

    private void Awake()
    {
        _start = Time.time;
    }

    private void OnCollisionEnter(Collision collision)
    {
        var other = collision.gameObject;

        var house = other.gameObject.GetComponentInChildren<House>();

        if (house)
        {
            house.HasBeenPooedOn();
        }

        Destroy(gameObject);
    }

    private void Update()
    {
        if (Time.time - _start > maxLifetime)
        {
            Destroy(gameObject);
        }
    }
}
