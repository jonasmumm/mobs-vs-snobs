using System.Collections;
using UnityEngine;

public class RandomSound : MonoBehaviour
{
    [SerializeField] AudioClip[] clips;
    [SerializeField] AudioSource source;

    void Awake()
    {
        StartCoroutine(WaitAndPlay());
    }

    IEnumerator WaitAndPlay()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(5f, 16f));

            source.clip = clips[UnityEngine.Random.Range(0, clips.Length)];
            source.Play();
        }
    }
}
