using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] Button button;

    private void Awake()
    {
        button.onClick.AddListener(StartLevel);
    }

    private void StartLevel()
    {
        Game.OpenScene<CinemaScene>(true, v => v.SetData(CinemaScene.Type.Intro, () =>
        {
            Game.OpenScene<PlayScene>(true);
            Game.CloseScene<CinemaScene>(false);
        }));
        Game.CloseScene<MainMenu>(false);
    }
}
